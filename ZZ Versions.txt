20033	Fixed Razorgore so it doesn't reset mid fight.(will have to force reset with ctrl-click on wipe)
	Went back to old version of CommonAuras, should fix random error on zeppelin crash

20032	Fixed Gehennas for real, BethyServer tested
	Fixed Baron Geddon inferno trigger, BethyServer tested. Need to confirm the trigger on Kronos.
	Fixed Shazzrah CS, Curse, BethyServer tested. Need to confirm triggers on Kronos

20031 	Fixed Gehennas	One "earliestCurse" was remaining, replaced with nextCurse.
			Confirmed no occurence of "earliestCurse" or "latestCurse" remain.
	Attempted to fix Baron Geddon	Only the first inferno timer happens. Changed ThrottleSync (line 196) to 15, as it was over the actual inferno timer.
			Deleted deletion of bar (since CD bar should always be done) and edited inferno sync and timer to what i think it should be.
			Fixed a typo in the bomb section, let's see how that goes...
			Shortened LivingBomb CD as per data.
			Adjusted nextIgnite CD as per data.

20030 Release of Relar version.
Adjusted all timers that seemed off.
Disabled icon placement on C'thun's fight (line 556), ppl can now enable icon placement risk-free.